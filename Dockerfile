FROM alpine:latest

ENV VERSION=v1.0.0

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN apk add --no-cache lua-turbo lua-redis lua-hashids && \
	mkdir -p /var/www/localhost/htdocs && \
	wget -qO- https://github.com/alpinelinux/turbo-paste/archive/$VERSION.tar.gz | \
	tar -zx --strip-components=1 -C /var/www/localhost/htdocs

WORKDIR /var/www/localhost/htdocs

USER 1000:1000

ENTRYPOINT ["./turbo-paste.lua"]
